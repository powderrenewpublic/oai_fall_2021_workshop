# Lab 1-bis RAN: run gNB and nrUE with a noisy channel
## Overview

Once you have run the `RF simulator` with perfect channel (no noise), we will try to add and modify a noisy air channel.

## Prerequisites

Please complete the LAB-1.

## Build an embedded TELNET server

Within the OAI modems, a telnet server can be instantiated. It allows gathering stats, modify some parameters while the modem is running.

More details about the telnet server is available in the OAI code repository in this [folder](https://gitlab.eurecom.fr/oai/openairinterface5g/-/tree/develop/common/utils/telnetsrv/DOC).

Let's build it, in the same shell terminal you used to build:

```bash
cd ~/openairinterface5g/cmake_targets
./build_oai --build-lib telnetsrv --ninja
```

Let's check that the build system created additional shared libraries:

```bash
ls ran_build/build/libtelnetsrv*
ran_build/build/libtelnetsrv.so  ran_build/build/libtelnetsrv_5Gue.so  ran_build/build/libtelnetsrv_enb.so
```

## Start the monolithic gNodeB

I did not stop the `5G-CN`. If you did, you need to restart it as explained in LAB1.

```bash
cd ~/openairinterface5g/cmake_targets
/local/repository/bin/start-gnb.sh
```

We are starting the `gNB` with the same options as for Lab1.

## Start the NR-UE

First steps, we need to update the `ue.conf` file.

In an additional terminal shell:

1. Copy the channel model configuration file
```bash
cd ~/openairinterface5g/cmake_targets
sudo cp ../ci-scripts/conf_files/channelmod_rfsimu.conf /local/repository/etc/
```
2. Edit the `ue.conf` file by adding one line at the end of file
```bash
$ tail -3 /local/repository/etc/ue.conf 
}

@include "channelmod_rfsimu.conf"
```

We've added this line that include this 2nd file.

Now we will use additional options to enable channel model:

```bash
cd ~/openairinterface5g/cmake_targets
sudo RFSIMULATOR=127.0.0.1 ./ran_build/build/nr-uesoftmodem -r 106 --numerology 1 --band 78 -C 3619200000 \
   --rfsim --sa --nokrnmod -d -O /local/repository/etc/ue.conf \
   --rfsimulator.options chanmod --telnetsrv
```

We've added:

*  `--telnetsrv` to start the TELNET server within the NR-UE
*  `--rfsimulator.options chanmod` to enable the channel model on the reception path to the NR-UE (ie Downlink)

Once you get both NR scopes, you should see:

* on the `gNB`-Scope, no changes since the Uplink is still with a perfect channel model
* on the `NR-UE`-Scope, the constellation should have a noisy [look](./images/100-Lab1-UE-Noise-Start.png)

## Access the telnet server

On a 3rd terminal:

```bash
$ telnet 127.0.0.1 9090
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.
```

Type `Enter`, you should get the prompt:

```bash
$ telnet 127.0.0.1 9090
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.

softmodem_5Gue>
```

Type the `channelmod` help:

```bash
softmodem_5Gue> channelmod help
channelmod commands can be used to display or modify channel models parameters
channelmod show predef: display predefined model algorithms available in oai
channelmod show current: display the currently used models in the running executable
channelmod modify <model index> <param name> <param value>: set the specified parameters in a current model to the given value
                  <model index> specifies the model, the show current model command can be used to list the current models indexes
                  <param name> can be one of "riceanf", "aoa", "randaoa", "ploss", "noise_power_dB", "offset", "forgetf"
```

Check the current noise:

```bash
softmodem_5Gue> channelmod show current
model 0 rfsimu_channel_enB0 type AWGN: 
----------------
model owner: rfsimulator
nb_tx: 1    nb_rx: 1    taps: 1 bandwidth: 0.000000    sampling: 61440000.000000
channel length: 1    Max path delay: 0.000000   ricean fact.: 0.000000    angle of arrival: 0.000000 (randomized:No)
max Doppler: 0.000000    path loss: 0.000000  noise: -10.000000 rchannel offset: 0    forget factor; 0.000000
Initial phase: 0.000000   nb_path: 10 
taps: 0   lin. ampli. : 1.000000    delay: 0.000000 
model 1 rfsimu_channel_ue0 type AWGN: 
----------------
model owner: not set
nb_tx: 1    nb_rx: 1    taps: 1 bandwidth: 0.000000    sampling: 61440000.000000
channel length: 1    Max path delay: 0.000000   ricean fact.: 0.000000    angle of arrival: 0.000000 (randomized:No)
max Doppler: 0.000000    path loss: 0.000000  noise: 0.000000 rchannel offset: 0    forget factor; 0.000000
Initial phase: 0.000000   nb_path: 10 
taps: 0   lin. ampli. : 1.000000    delay: 0.000000 
```

The `Noise Level` is at `-10 dB` on the channel model from the `gNB`.

Let's increase it:

```bash
softmodem_5Gue> channelmod modify 0  noise_power_dB -5
model owner: rfsimulator
nb_tx: 1    nb_rx: 1    taps: 1 bandwidth: 0.000000    sampling: 61440000.000000
channel length: 1    Max path delay: 0.000000   ricean fact.: 0.000000    angle of arrival: 0.000000 (randomized:No)
max Doppler: 0.000000    path loss: 0.000000  noise: -5.000000 rchannel offset: 0    forget factor; 0.000000
Initial phase: 0.000000   nb_path: 10 
taps: 0   lin. ampli. : 1.000000    delay: 0.000000 
```

On the NR-UE Scope, you should see that the constellation has a more noisy [look](./images/101-Lab1-UE-Noise-More-Noise.png).

You will find more details on the channel model options [here](https://gitlab.eurecom.fr/oai/openairinterface5g/-/blob/develop/openair1/SIMULATION/TOOLS/DOC/rtusage.md).

Once the presentation on 5G F1 CU/DU split is done, let's jump into the [Lab2 hands-on session](./Lab-2-RAN.md).
