# Lab 2 RAN: testing the new 5G F1 CU/DU split
## Overview
The primary purpose of this lab is to guide participants through a hands-on session using the OpenAirInterface RAN environment. We will do this by:

1. Re-using the LAB1 environment
2. Switching to a different branch
3. Re-compiling and running a different configuration

## Prerequisites

1. Completion of the LAB1 RAN.

## Stop properly the LAB1

To stop the gNB and the NR-UE softmodems, push the `CTRL` + `C` keys.

Let stop the `CN5G` also in order to leave all CPU resources to re-compile.

```
cd /opt/oai-cn5g-fed/docker-compose
sudo python3 ./core-network.py --type stop-mini --fqdn no --scenario 1
```

Dont' worry! We will re-deploy it again later!

## Prepare the RAN workspace for F1 lab

We recommend that you use the terminal you already used to build the first time. In that case, the first 3 commands are not needed.

Otherwise if you start from a fresh terminal.

```
bash
cd ~/openairinterface5g
source oaienv

# Clear the RAN workspace of all build artifacts
sudo git clean -x -d -ff

# Fetch and switch to the F1 development branch
git fetch origin NR_F1C_F1U_extensions:NR_F1C_F1U_extensions --depth 1
git checkout NR_F1C_F1U_extensions

# Same build command
cd cmake_targets
./build_oai --gNB --nrUE -w SIMU --build-lib nrscope --ninja
```

## Re-Deploy the OAI CN5G

Once again you should be reusing the same terminal as for LAB1.

Otherwise from a fresh terminal.

```
bash
cd /opt/oai-cn5g-fed/docker-compose
sudo python3 ./core-network.py --type start-mini --fqdn no --scenario 1
```

## Start the CU and DU instances

First let us launch a `tshark` to capture the packets on the F1 interface:

**CAUTION: please use the same syntax or your `PCAP` file will be huge!**

```
nohup sudo tshark -i lo -f "sctp or port 2153" -w /tmp/test-f1.pcap > /dev/null 2>&1 &
```

At this point we will need 4 terminals:

1. For the `Centralized Unit` (aka `CU`)
2. For the `Distributed Unit` (aka `DU`)
3. For the `NR-UE`
4. To test the traffic

Normally the VNC session starts with that many shells.

*Shell 1*
```
bash
cd ~/openairinterface5g/cmake_targets/
sudo RFSIMULATOR=server ./ran_build/build/nr-softmodem --rfsim --sa -O /local/repository/etc/cu.conf
```

**Note that I did not use the `-d` option here!**

*Shell 2*
```
bash
cd ~/openairinterface5g/cmake_targets/
sudo RFSIMULATOR=server ./ran_build/build/nr-softmodem --rfsim --sa -d -O /local/repository/etc/du.conf
```

**Note that here I used the `-d` option!**

These 2 configuration files are based on the ones present in the repository:

- `ci-scripts/conf_files/gNB_SA_CU.conf`
- `ci-scripts/conf_files/gNB_SA_DU.conf`

Note that I used configuration files from the `ci-scripts/conf_files` folder: they are the most up-to-date since they are used by every-day CI runs.

*Shell 3*
```
bash
cd ~/openairinterface5g/cmake_targets
# Same Command as in LAB 1
sudo RFSIMULATOR=127.0.0.1 ./ran_build/build/nr-uesoftmodem -r 106 --numerology 1 --band 78 -C 3619200000 \
   --rfsim --sa --nokrnmod -d -O /local/repository/etc/ue.conf
```

As in LAB1, the UE will associate with the network, as indicated by log output from the AMF, gNodeB, and UE processes, and the UE soft-scope will open up showing various plots.

You can generate some traffic by pinging the external data network service that gets deployed along with the OAI 5G core network functions:

*Shell 4*
```
$ ping -I oaitun_ue1 192.168.70.135
PING 192.168.70.135 (192.168.70.135) from 12.1.1.129 oaitun_ue1: 56(84) bytes of data.
64 bytes from 192.168.70.135: icmp_seq=1 ttl=63 time=33.9 ms
64 bytes from 192.168.70.135: icmp_seq=2 ttl=63 time=67.9 ms
64 bytes from 192.168.70.135: icmp_seq=3 ttl=63 time=67.0 ms
64 bytes from 192.168.70.135: icmp_seq=4 ttl=63 time=76.8 ms
64 bytes from 192.168.70.135: icmp_seq=5 ttl=63 time=76.8 ms
64 bytes from 192.168.70.135: icmp_seq=6 ttl=63 time=27.1 ms
64 bytes from 192.168.70.135: icmp_seq=7 ttl=63 time=24.6 ms
64 bytes from 192.168.70.135: icmp_seq=8 ttl=63 time=20.1 ms
64 bytes from 192.168.70.135: icmp_seq=9 ttl=63 time=20.1 ms
64 bytes from 192.168.70.135: icmp_seq=10 ttl=63 time=56.3 ms
64 bytes from 192.168.70.135: icmp_seq=11 ttl=63 time=57.1 ms
64 bytes from 192.168.70.135: icmp_seq=12 ttl=63 time=56.7 ms
64 bytes from 192.168.70.135: icmp_seq=13 ttl=63 time=59.2 ms
64 bytes from 192.168.70.135: icmp_seq=14 ttl=63 time=15.7 ms
...
--- 192.168.70.135 ping statistics ---
54 packets transmitted, 54 received, 0% packet loss, time 53031ms
rtt min/avg/max/mdev = 6.888/44.506/88.239/22.669 ms
```

We can also have a look at the PCAP file:

```
sudo tshark -r /tmp/test-f1.pcap
    1 0.000000000    127.0.0.1 → 127.0.0.4    SCTP 114 INIT 
    2 0.000071009    127.0.0.4 → 127.0.0.1    SCTP 338 INIT_ACK 
    3 0.000097105    127.0.0.1 → 127.0.0.4    SCTP 310 COOKIE_ECHO 
    4 0.000156911    127.0.0.4 → 127.0.0.1    SCTP 50 COOKIE_ACK 
    5 0.001403441    127.0.0.1 → 127.0.0.4    F1AP 258 F1SetupRequest, MIB, SIB1[UNKNOWN PER: too long integer(per_normally_small_nonnegative_whole_number)][Malformed Packet]
    6 0.001432094    127.0.0.4 → 127.0.0.1    SCTP 62 SACK 
    7 0.002266238    127.0.0.4 → 127.0.0.1    F1AP 94 F1SetupResponse
    8 0.002290716    127.0.0.1 → 127.0.0.4    SCTP 62 SACK 
    9 0.002770180    127.0.0.4 → 127.0.0.1    F1AP 118 GNBCUConfigurationUpdate SIB2[UNKNOWN PER: too many extensions][Malformed Packet]
   10 0.004888625    127.0.0.1 → 127.0.0.4    F1AP 94 SACK , GNBCUConfigurationUpdateAcknowledge
   11 0.205303151    127.0.0.4 → 127.0.0.1    SCTP 62 SACK 
   12 1.789341996    127.0.0.4 → 155.98.36.85 SCTP 98 HEARTBEAT 
   13 1.789389774    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
   14 2.557325182    127.0.0.4 → 172.17.0.1   SCTP 98 HEARTBEAT 
   15 2.557353324    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
   16 3.581338500    127.0.0.4 → 192.168.70.129 SCTP 98 HEARTBEAT 
   17 3.581373848    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
   18 18.501488137    127.0.0.1 → 127.0.0.4    F1AP/NR RRC 218 RRC Setup Request
   19 18.504377723    127.0.0.4 → 127.0.0.1    F1AP/NR RRC 222 SACK , RRC Setup
   20 18.522046253    127.0.0.1 → 127.0.0.4    F1AP/NR RRC/NAS-5GS 158 SACK , RRC Setup Complete, Registration request MAC=0x00000000
   21 18.556603732    127.0.0.4 → 127.0.0.1    F1AP/NR RRC/NAS-5GS 166 SACK , DL Information Transfer, Authentication request MAC=0x00000000
   22 18.616504676    127.0.0.1 → 127.0.0.4    F1AP/NR RRC/NAS-5GS 142 SACK , UL Information Transfer, Authentication response MAC=0x00000000
   23 18.621454532    127.0.0.4 → 127.0.0.1    F1AP/NR RRC/NAS-5GS 146 SACK , DL Information Transfer, Security mode command MAC=0x00000000
   24 18.711489021    127.0.0.1 → 127.0.0.4    F1AP/NR RRC/NAS-5GS 170 SACK , UL Information Transfer MAC=0x00000000
   25 18.717988737    127.0.0.4 → 127.0.0.1    F1AP/NR RRC 126 SACK , Security Mode Command MAC=0x1dd8eede
   26 18.799295879    127.0.0.1 → 127.0.0.4    F1AP/NR RRC 118 SACK , Security Mode Complete MAC=0x6d36ae70
   27 18.800202147    127.0.0.4 → 127.0.0.1    F1AP/NR RRC 130 SACK , UE Capability Enquiry MAC=0xb8982878
   28 18.885052816    127.0.0.1 → 127.0.0.4    F1AP/NR RRC 130 SACK , UE Capability Information MAC=0x66ee2c64
   29 18.886341124    127.0.0.4 → 127.0.0.1    F1AP/NR RRC/NAS-5GS 190 SACK , RRC Reconfiguration MAC=0xeb58d603
   30 18.970733251    127.0.0.1 → 127.0.0.4    F1AP/NR RRC 118 SACK , RRC Reconfiguration Complete MAC=0x84143c58
   31 19.173284134    127.0.0.4 → 127.0.0.1    SCTP 62 SACK 
   32 19.910179183    127.0.0.1 → 127.0.0.4    F1AP/NR RRC/NAS-5GS 114 UL Information Transfer MAC=0xfd3da89d
   33 20.113284179    127.0.0.4 → 127.0.0.1    SCTP 62 SACK 
   34 20.113330475    127.0.0.1 → 127.0.0.4    F1AP/NR RRC/NAS-5GS 138 UL Information Transfer MAC=0x3bc40865
   35 20.132370814    127.0.0.4 → 127.0.0.1    F1AP/NR RRC/NAS-5GS 254 SACK , RRC Reconfiguration MAC=0xaf7020c3
   36 20.158045421    127.0.0.1 → 127.0.0.4    F1AP/NR RRC 118 SACK , RRC Reconfiguration Complete MAC=0x8ea7d6cd
   37 20.160048739    127.0.0.4 → 127.0.0.1    F1AP 206 SACK , UEContextSetupRequest
   38 20.161758244    127.0.0.1 → 127.0.0.4    F1AP 162 SACK , UEContextSetupResponse
   39 20.365283435    127.0.0.4 → 127.0.0.1    SCTP 62 SACK 
   40 20.568248630    127.0.0.3 → 127.0.0.4    UDP 101 2153 → 2153 Len=59
   41 24.389214777    127.0.0.3 → 127.0.0.4    UDP 101 2153 → 2153 Len=59
   42 25.866560491    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   43 25.867048088    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   44 26.860213857    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   45 26.860595618    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   46 27.885933681    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   47 27.886340424    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   48 28.875656512    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   49 28.875995774    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   50 29.862870132    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   51 29.863188383    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   52 30.930666007    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   53 30.931018153    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   54 31.539021498    127.0.0.3 → 127.0.0.4    UDP 101 2153 → 2153 Len=59
   55 31.873477583    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   56 31.873789586    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   57 32.858901487    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   58 32.859234736    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   59 33.934319416    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   60 33.934698391    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   61 34.301310176    127.0.0.4 → 155.98.36.85 SCTP 98 HEARTBEAT 
   62 34.301331737    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
   63 34.301352722    127.0.0.4 → 172.17.0.1   SCTP 98 HEARTBEAT 
   64 34.301394640    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
   65 34.921628481    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   66 34.921946051    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   67 35.866810807    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   68 35.867186714    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   69 36.349318755    127.0.0.4 → 192.168.70.129 SCTP 98 HEARTBEAT 
   70 36.349374563    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
   71 36.937765974    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   72 36.938153568    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   73 37.931428634    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   74 37.931813448    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   75 38.913577884    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   76 38.913886921    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   77 39.866620636    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   78 39.866930618    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   79 40.869213852    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   80 40.869568776    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   81 41.891945528    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   82 41.892264069    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   83 42.890314064    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   84 42.890627119    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   85 43.923393310    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   86 43.923749538    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   87 44.919995645    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   88 44.920358314    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   89 45.088887383    127.0.0.3 → 127.0.0.4    UDP 101 2153 → 2153 Len=59
   90 45.920035034    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   91 45.920405359    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   92 46.892290746    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   93 46.892622526    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   94 47.911569081    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   95 47.911947445    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   96 48.965595444    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   97 48.965918454    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
   98 49.881733773    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
   99 49.882024813    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  100 50.685288575    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT 
  101 50.685297185    127.0.0.4 → 127.0.0.1    SCTP 98 HEARTBEAT 
  102 50.685314028    127.0.0.4 → 127.0.0.1    SCTP 98 HEARTBEAT_ACK 
  103 50.685322541    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
  104 50.950776534    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  105 50.951097492    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  106 51.910447293    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  107 51.910782160    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  108 52.953478097    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  109 52.953778361    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  110 53.934209410    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  111 53.934524174    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  112 54.916003087    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  113 54.916384227    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  114 55.972019104    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  115 55.972414511    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  116 56.966404394    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  117 56.966804279    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  118 57.920763098    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  119 57.921308325    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  120 58.901762992    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  121 58.902111805    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  122 59.954409481    127.0.0.3 → 127.0.0.4    UDP 137 2153 → 2153 Len=95
  123 59.954740319    127.0.0.4 → 127.0.0.3    UDP 138 2153 → 2153 Len=96
  124 65.021305740    127.0.0.4 → 172.17.0.1   SCTP 98 HEARTBEAT 
  125 65.021342943    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
  126 65.021348599    127.0.0.4 → 155.98.36.85 SCTP 98 HEARTBEAT 
  127 65.021394412    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
  128 69.117306978    127.0.0.4 → 192.168.70.129 SCTP 98 HEARTBEAT 
  129 69.117383884    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
  130 73.302017624    127.0.0.3 → 127.0.0.4    UDP 101 2153 → 2153 Len=59
  131 83.453294153    127.0.0.4 → 127.0.0.1    SCTP 98 HEARTBEAT 
  132 83.453308319    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT 
  133 83.453328479    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
  134 83.453337600    127.0.0.4 → 127.0.0.1    SCTP 98 HEARTBEAT_ACK 
  135 95.741326089    127.0.0.4 → 172.17.0.1   SCTP 98 HEARTBEAT 
  136 95.741375390    127.0.0.1 → 127.0.0.4    SCTP 98 HEARTBEAT_ACK 
  137 97.488316008    127.0.0.1 → 127.0.0.4    SCTP 54 SHUTDOWN 
  138 97.488372095    127.0.0.4 → 127.0.0.1    SCTP 50 SHUTDOWN_ACK 
  139 97.488386549    127.0.0.1 → 127.0.0.4    SCTP 50 SHUTDOWN_COMPLETE 
```

The `UDP` packets on the port `2153` are the ping packets transported over the `F1-U` interface.

Et voila!

**Note that we are using a very recent version of `tshark` with 5G support!**
```
tshark --version
TShark (Wireshark) 3.4.8 (Git v3.4.8 packaged as 3.4.8-1~ubuntu18.04.0+wiresharkdevstable1)
```

How to install it:

```
sudo add-apt-repository ppa:wireshark-dev/stable
sudo apt update
sudo apt install wireshark tshark
```

## Ending the lab

As discussed in section `Stop properly the LAB1`, `CTRL` + `C` to stop CU, DU and NR-UE. Undeploy the CN5G.

**PLEASE DO NOT FORGET TO KILL THE TSHARK COMMAND!**

```
sudo killall tshark
```
