# Lab 1 RAN: build/run gNB and nrUE
## Overview
The primary purpose of this lab is to guide participants through a hands-on session using the OpenAirInterface RAN environment. We will do this by:

1. Setting up a basic "virtual lab" environment in the POWDER mobile and wireless platform.
2. Downloading and compiling the OAI RAN code base to instantiate an OAI simulated RAN configuration.
3. Interacting with the instantiated experiment.

In addition we will demonstrate how the POWDER platform can be used to realize more realistic RAN topologies, including over-the-air configurations, in which OAI software can be experimented with.

## Prerequisites
~~1. Join the OAI Fall 2021 Workshop project on the POWDER platform: [https://www.powderwireless.net/signup.php?pid=OAI2021FallWS](https://www.powderwireless.net/signup.php?pid=OAI2021FallWS)~~

OAI2021FallWS was a project we specifically set up for use (only) during the OAI workshop. In order to do these hands-on labs you will need to [create your own project](https://docs.powderwireless.net/users.html) in POWDER. I.e., the rest of the instructions should work as is in any POWDER project. 

## Set up your virtual lab on POWDER
Note: You will have to wait to get your POWDER project join request approved before proceeding with this part.

In POWDER we call the "virtual lab" you are about to create an "experiment".

1. Select this [POWDER Profile (oai-5g-e2e-rfsim)](https://www.powderwireless.net/p/OAI2021FallWS/oai-5g-e2e-rfsim) which specifies the platform resources requested for our "virtual lab" configuration, i.e., a single compute node running `Ubuntu 18` and pre-provisioned to run the dockerized OAI 5G core network functions. It also provides a couple of configuration files, so we can avoid requiring you to create/edit files using the in-browser VNC session we'll be using for this Lab. Select "Next".
1. (Optional) Provide a name for your experiment. (For the purposes of this hands-on session, it's best to leave the name entry blank. That way the POWDER portal will automatically choose a random name that is guaranteed not to collide with other experiment names). Select "Next".
1. Select "Finish"

Please **BE PATIENT** after clicking the "Finish" button, since, due to the large load on the POWDER portal during this hands-on session, it may take a few minutes before any progress is indicated in the web UI. **DO NOT** refresh the page and/or try to instantiate another experiment during this period.

## Access your experiment/virtual lab
Note: You have to wait till the POWDER portal shows your experiment status in green as "Your experiment is ready!" before proceeding with this section.

1. On the POWDER portal navigate to the experiment you just started. ("Experiments" -> "My Experiments"; Click on the experiment name.) 
1. Select the "List view" tab.
1. In the resource table shown, click on the gear icon in the last column of the "node" row and selct "Shell". (There is only one row in this experiment list view table.)  
1. This will open up a shell window on your compute node. Excute the command below to install noVNC on your node:
```
/share/powder/runvnc.sh
```
When prompted provide a temporary password for your noVNC server.
Select "n" when asked whether you want to create a view-only password.
1. Create a new browser window or tab and copy the URL provided at the end of the process.
   * The noVNC server uses a self-signed certificate, so your browser will likely warn you not to proceed.
   * Follow your browser specific exception process to continue to the copied URL.
   * You may have to use a VPN if your network configuration does not allow access to it.
1. Enter the temporary password you created in the previous step to gain access to your X-windows environment. 

At this point you should see a grid of four terminals. We'll use all of them for this tutorial:

1. To deploy and follow logs for the 5G core network
1. For running the gNodeB
1. For running the NR-UE
1. To generate traffic after the UE attaches to the network

## Build OAI RAN
Note: The commands below will be executed in one of the X-window terminals from the previous step. 

We are basically following the [TESTING_5GSA_setup.md](https://gitlab.eurecom.fr/oai/openairinterface5g/-/blob/develop/doc/TESTING_5GSA_setup.md) instructions from OAI. For convenience the instructions are repeated here (with some minor differences to suite our purpose).

First, clone the repository. Note that we are using a local mirror of the OAI repository and only making shallow clones of specific branches/tags in order to speed up the cloning process and reduce pressure on the server. Also, since we know that the copy/paste functionality can be a little finicky in the browser-based VNC terminals used for this tutorial, we've provided a few helper scripts for running some of the longer commands.

```
bash
# We will use a dedicated tag since one package is sometimes difficult to install
git clone --branch 2021.w46-powder --depth 1 https://gitlab.flux.utah.edu/powder-mirror/openairinterface5g ~/openairinterface5g
```
**Or**, use a helper script:
```
bash
/local/repository/bin/clone-oai.sh
```
In the latter case, the helper script will print out the command it's about to run, and execute it when you press enter/return.

Next, install dependencies and build OAI:
```
cd ~/openairinterface5g
source oaienv
cd cmake_targets/

# Even if we won't be using USRP in this experiment, let's install UHD
export BUILD_UHD_FROM_SOURCE=True
export UHD_VERSION=3.15.0.0

# The next command takes around 8 minutes
# This command SHALL be done ONCE in the life of your server
./build_oai -I -w USRP

# Let's build now the gNB and nrUE soft modems
# The next command takes around 6 minutes
./build_oai --gNB --nrUE -w SIMU --build-lib nrscope --ninja
```

## Start the "minimal" OAI 5G core network deployment
```
# The next 2 commands are done to make the containers visible from an external server
# You SHALL do these if your EPC containers are not on the same server as your gNB
# Not the case in our experiment. But good practice.
sudo sysctl net.ipv4.conf.all.forwarding=1
sudo iptables -P FORWARD ACCEPT

# Let's deploy now
cd /opt/oai-cn5g-fed/docker-compose
sudo python3 ./core-network.py --type start-mini --fqdn no --scenario 1
```
It will take the `core-network.py` script several seconds to deploy the network function containers and indicate that they are healthy.

```
...
[2021-11-24 09:39:33,916] root:DEBUG:  AMF, SMF and UPF are registered to NRF....
[2021-11-24 09:39:33,916] root:DEBUG:  Checking if SMF is able to connect with UPF....
[2021-11-24 09:39:34,075] root:DEBUG:  UPF receiving heathbeats from SMF....
[2021-11-24 09:39:34,075] root:DEBUG:  OAI 5G Core network is configured and healthy....
```

Use the same X-window terminal to follow the logs for the AMF so you can verify that the UE registers later on:
```
sudo docker logs -f oai-amf
...
[2021-11-24T16:41:33.348997] [AMF] [amf_app] [info ] |----------------------------------------------------UEs' information--------------------------------------------|
[2021-11-24T16:41:33.349004] [AMF] [amf_app] [info ] | Index |      5GMM state      |      IMSI        |     GUTI      | RAN UE NGAP ID | AMF UE ID |  PLMN   |Cell ID|
[2021-11-24T16:41:33.349010] [AMF] [amf_app] [info ] |----------------------------------------------------------------------------------------------------------------|
[2021-11-24T16:41:33.349016] [AMF] [amf_app] [info ] 
```

## Start the monolithic gNodeB
In another X-window terminal, run the monolithic gNodeB using the configuration file provided by the profile::
```
cd ~/openairinterface5g/cmake_targets
sudo RFSIMULATOR=server ./ran_build/build/nr-softmodem --rfsim --sa \
   -d -O /local/repository/etc/gnb.conf
```
**Or**, let a helper script type the command for you:
```
/local/repository/bin/start-gnb.sh
```

The `gNB` options are:
*  `--rfsim` to indicate no real Radio Equipement will be used and RF simulation will transport IQ samples though Ethernet link
*  `--sa` to indicate `STANDALONE` mode. If omitted, the gNB would be running in `NON-STANDALONE` mode (Currently not available w/ RF sim)
*  `-d` to start the gNB NR scope

Note: the configuration file is just a copy of `~/openairinterface5g/targets/PROJECTS/GENERIC-NR-5GC/CONF/gnb.sa.band78.fr1.106PRB.usrpb210.conf` that has been adjusted to use the tracking area code and mobile network code that the minimal OAI 5G core network deployment expects.

You will notice the gNodeB connecting to the AMF if you are watching oai-amf log. Also, the gNodeB soft-scope will open up and start showing various plots. It may block the view of other X-window terminals, but you can minimize X-windows by right-clicking on their top bars. You can also bring back X-windows by right-clicking on an empty area in the viewport and selecting from the list of hidden X-windows that pops up.

## Start the UE
In yet another X-window terminal, run the UE using the configuration file provided by the profile:

```
cd ~/openairinterface5g/cmake_targets
sudo RFSIMULATOR=127.0.0.1 ./ran_build/build/nr-uesoftmodem -r 106 --numerology 1 --band 78 -C 3619200000 \
   --rfsim --sa --nokrnmod -d -O /local/repository/etc/ue.conf
```
**Or**, use another helper script:
```
/local/repository/bin/start-nrue.sh
```

Note that some options are very similar to the `LTE-UE`:
*  `-r` for the number of resource blocks
*  `--band` to specify the band to operate
*  `-C` to specify the central carrier frequency

Note: the UE configuration file sets various credentials (IMSI, Ki, OPC, etc) to match records that exist in the minimal OAI 5G core network deployment.

The UE will associate with the network, as indicated by log output from the AMF, gNodeB, and UE processes, and the UE soft-scope will open up showing various plots.

## Generate some traffic
You can generate some traffic by pinging the external data network service that gets deployed along with the OAI 5G core network functions:

```
ping -I oaitun_ue1 192.168.70.135
PING 192.168.70.135 (192.168.70.135) from 12.1.1.129 oaitun_ue1: 56(84) bytes of data.
64 bytes from 192.168.70.135: icmp_seq=1 ttl=63 time=15.2 ms
64 bytes from 192.168.70.135: icmp_seq=2 ttl=63 time=24.3 ms
64 bytes from 192.168.70.135: icmp_seq=3 ttl=63 time=34.3 ms
64 bytes from 192.168.70.135: icmp_seq=4 ttl=63 time=64.5 ms
64 bytes from 192.168.70.135: icmp_seq=5 ttl=63 time=87.0 ms
64 bytes from 192.168.70.135: icmp_seq=6 ttl=63 time=22.6 ms
64 bytes from 192.168.70.135: icmp_seq=7 ttl=63 time=35.9 ms
64 bytes from 192.168.70.135: icmp_seq=8 ttl=63 time=73.6 ms
64 bytes from 192.168.70.135: icmp_seq=9 ttl=63 time=9.55 ms
64 bytes from 192.168.70.135: icmp_seq=10 ttl=63 time=17.4 ms
64 bytes from 192.168.70.135: icmp_seq=11 ttl=63 time=25.4 ms
64 bytes from 192.168.70.135: icmp_seq=12 ttl=63 time=52.7 ms
^C
--- 192.168.70.135 ping statistics ---
12 packets transmitted, 12 received, 0% packet loss, time 11011ms
rtt min/avg/max/mdev = 9.555/38.573/87.065/24.059 ms
```

Note that if I ping the container directly, the `Round-Trip-Time (RTT)` is way smaller:

```
ping -c 5 192.168.70.135
PING 192.168.70.135 (192.168.70.135) 56(84) bytes of data.
64 bytes from 192.168.70.135: icmp_seq=1 ttl=64 time=0.066 ms
64 bytes from 192.168.70.135: icmp_seq=2 ttl=64 time=0.060 ms
64 bytes from 192.168.70.135: icmp_seq=3 ttl=64 time=0.063 ms
64 bytes from 192.168.70.135: icmp_seq=4 ttl=64 time=0.051 ms
64 bytes from 192.168.70.135: icmp_seq=5 ttl=64 time=0.053 ms

--- 192.168.70.135 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4089ms
rtt min/avg/max/mdev = 0.051/0.058/0.066/0.010 ms
```

**It is the proof that the 1st traffic test went trough the whole OAI stack!**

If you have time, you can do the [Lab1-extension about channel model](./Lab-1bis-RAN.md).

If not, you may have to jump directly to the [Lab2 about 5G F1 CU/DU split](./Lab-2-RAN.md).
