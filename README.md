# Fall 2021 OpenAirInterface Workshop: Hands-On with the OAI Architects

The step-by-step instructions in these Labs were specifically developed by POWDER and OpenAirInterface to support the [Fall 2021 OpenAirInterface Workshop](https://openairinterface.org/fall-2021-openairinterface-workshop/). The instructions are detailed enough though to serve as a "self help" tutorial on OAI 5G RAN, assuming you have access to a POWDER project (or [create one](https://docs.powderwireless.net/users.html) if you don't). 

## Lab 1: 5G RAN - Simulation without noise

1. Instantiate experiment on POWDER for Lab 1 and OAI RAN Overview (Raphael Defosseux) [Slides](./presentations/RAN-LAB1-Presentation.pdf)
2. POWDER overview (Kobus Van der Merwe) [Slides](https://docs.google.com/presentation/d/1XM_7OhgtfEg0_GxWEsiwDJ6xfoQ2zcCgTKayZAkHWAw/edit?usp=sharing)
3. [Lab 1 hands-on](./Lab-1-RAN.md) (Dustin Maas)
4. From simulated RF to real radios: POWDER profile mechanism (Kirk Webb)
5. From simulated RF to real radios: POWDER based demos (Dustin Maas)
- Paired radio workbench
- Indoor OTA ([video](https://youtu.be/eV2gotjIP98))

## Lab 1 - Extension : 5G RAN - Simulation with a channel model

1. [Lab 1 Extension](./Lab-1bis-RAN.md) (Raphael Defosseux)

## Lab 2: 5G RAN - F1 CU/DU split

1. [Lab 2 hands-on](./Lab-2-RAN.md) (Laurent Thomas / Raphael Defosseux) [Slides](./presentations/LAB2.pdf)
